package jp.co.kenshu.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.comment.text.CommentTextDto;
import jp.co.kenshu.dto.text.TextDto;
import jp.co.kenshu.entity.CommentText;
import jp.co.kenshu.entity.Text;
import jp.co.kenshu.mapper.CommentTextMapper;
import jp.co.kenshu.mapper.TextMapper;


@Service
public class TextService {

	@Autowired
	private TextMapper textMapper;

	public TextDto getText(int id) {

		TextDto dto = new TextDto();
		Text entity = textMapper.getText(id);
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	//全件取得
	public List<TextDto> getTextAll() {
		List<Text> textList = textMapper.getTextAll();
		List<TextDto> resultList = convertToDto(textList);
		return resultList;
	}

	private List<TextDto> convertToDto(List<Text> textList) {
		List<TextDto> resultList = new LinkedList<>();
		for (Text entity : textList) {
			TextDto dto = new TextDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	//投稿送信機能
	public int insertText(String memo) {
		int count2 = textMapper.insertText(memo);
		return count2;
	}

	@Autowired
	private CommentTextMapper commentTextMapper;

	//コメント取得
	public List<CommentTextDto> getCommentTextAll() {
		List<CommentText> commentTextList = commentTextMapper.getCommentTextAll();
		List<CommentTextDto> resultCommentList = convertToCommentDto(commentTextList);
		return resultCommentList;
	}

	private List<CommentTextDto> convertToCommentDto(List<CommentText> commentTextList) {
		List<CommentTextDto> resultCommentList = new LinkedList<>();
		for (CommentText entity : commentTextList) {
			CommentTextDto dto = new CommentTextDto();
			BeanUtils.copyProperties(entity, dto);
			resultCommentList.add(dto);
		}
		return resultCommentList;
	}



	//コメント投稿送信機能
	public int insertCommentText(String comment , int MemoId) {
		int count1 = commentTextMapper.insertCommentText(comment , MemoId);
		return count1;
	}


}