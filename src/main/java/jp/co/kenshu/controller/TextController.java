package jp.co.kenshu.controller;

import java.util.List;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.comment.form.TextCommentForm;
import jp.co.kenshu.dto.comment.text.CommentTextDto;
import jp.co.kenshu.dto.text.TextDto;
import jp.co.kenshu.form.TextForm;
import jp.co.kenshu.service.TextService;

@Controller
public class TextController {

	@Autowired
	private TextService textService;

	@RequestMapping(value = "/20ch/{id}", method = RequestMethod.GET)
	public String text(Model model, @PathVariable int id) {

		TextDto text = textService.getText(id);
		model.addAttribute("title", "20chの投稿一覧情報を表示します");
		model.addAttribute("text", text);
		return "text";
	}

	//全件取得
	@RequestMapping(value = "/20ch/", method = RequestMethod.GET)
	public String textAll(Model model) {
		List<TextDto> texts = textService.getTextAll();
		model.addAttribute("title", "20chの投稿一覧画面");
		model.addAttribute("title2", "投稿一覧");
		model.addAttribute("texts", texts);

		//コメント取得
		List<CommentTextDto> commentTexts = textService.getCommentTextAll();
		model.addAttribute("comments", commentTexts);

		//コメント投稿送信機能
		TextCommentForm commentForm = new TextCommentForm();
		model.addAttribute("textCommentForm", commentForm);

		return "textAll";
	}
	//コメント投稿送信機能
	@RequestMapping(value = "/20ch/", method = RequestMethod.POST)
	public String textAll(@ModelAttribute TextCommentForm commentForm, Model model) {
		int count1 = textService.insertCommentText(commentForm.getComment() , commentForm.getMemoId());
		Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count1 + "件です。");
		return "redirect:/20ch/";
	}


	//投稿送信機能
	@RequestMapping(value = "/20ch/insert/input/", method = RequestMethod.GET)
	public String textInsert(Model model) {
		TextForm form = new TextForm();
		model.addAttribute("textForm", form);
		model.addAttribute("message", "20chの投稿画面");
		return "textInsert";
	}

	@RequestMapping(value = "/20ch/insert/input/", method = RequestMethod.POST)
	public String textInsert(@ModelAttribute TextForm form, Model model) {
		int count2 = textService.insertText(form.getMemo());
		Logger.getLogger(TextController.class).log(Level.INFO, "挿入件数は" + count2 + "件です。");
		return "redirect:/20ch/";
	}


}






/*
private List<TextDto> textList = new ArrayList<>();

@RequestMapping(value = "20ch/text/list", method = RequestMethod.GET)
public String list(Model model) {
	model.addAttribute("title", "20chの投稿一覧情報を表示します");
	TextListForm form = new TextListForm();
	model.addAttribute("textListForm", form);
	model.addAttribute("textList", textList);
	return "text/list";
}

@RequestMapping(value = "20ch/text/list", method = RequestMethod.POST)
public String list(@ModelAttribute TextListForm form, Model model, BindingResult result) {

	TextDto dto = new TextDto();
	BeanUtils.copyProperties(form, dto);
	textList.add(dto);
	model.addAttribute("title", "20chの投稿一覧情報を表示します");
	model.addAttribute("message", form.getMemo() + "を投稿しました。");
	model.addAttribute("textListForm", new TextListForm());
	model.addAttribute("textList", textList);
	return "text/list";
}
 */