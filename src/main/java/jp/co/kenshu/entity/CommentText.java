package jp.co.kenshu.entity;

public class CommentText {

	private Integer id;
	private String comment;
	private Integer MemoId;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getMemoId() {
		return MemoId;
	}
	public void setMemoId(Integer MemoId) {
		this.MemoId = MemoId;
	}
}
