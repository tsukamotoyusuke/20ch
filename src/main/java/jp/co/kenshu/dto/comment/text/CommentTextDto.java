package jp.co.kenshu.dto.comment.text;

public class CommentTextDto {

	private Integer id;
	private String memo;
	private String comment;
	private Integer memoId;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getMemo() {
		return memo;
	}
	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public Integer getMemoId() {
		return memoId;
	}
	public void setMemoId(Integer MemoId) {
		this.memoId = MemoId;
	}

}
