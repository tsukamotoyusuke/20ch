package jp.co.kenshu.comment.form;

public class TextCommentForm {

	private int id;

	private String comment;
	private int memoId;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getMemoId() {
		return memoId;
	}
	public void setMemoId(int MemoId) {
		this.memoId = MemoId;
	}

}

