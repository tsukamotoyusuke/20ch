package jp.co.kenshu.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.CommentText;

public interface CommentTextMapper {

	CommentText getCommentText(int MemoId);


	//コメント取得
	List<CommentText> getCommentTextAll();

	//コメント投稿
	int insertCommentText(@Param("comment")String comment , @Param("MemoId") int MemoId);

}
