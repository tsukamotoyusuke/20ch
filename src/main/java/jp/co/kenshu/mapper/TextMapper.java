package jp.co.kenshu.mapper;

import java.util.List;

import jp.co.kenshu.entity.Text;

public interface TextMapper {
	Text getText(int id);


	//全件取得
	List<Text> getTextAll();

	//投稿
	int insertText(String memo);

}
