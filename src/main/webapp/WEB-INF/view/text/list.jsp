<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><c:out value="${title}"></c:out></title>
</head>
<body>
	<h1>
		<c:out value="${title}"></c:out>
	</h1>
	<p>
		<c:out value="${message}"></c:out>
	</p>
	<form:form modelAttribute="textListForm">
		<table>
			<tbody>
				<tr>
					<form:label path="memo">投稿内容</form:label><br />
					<form:textarea path="memo" cols="80" rows="5" />
				</tr>
			</tbody>
		</table>
		<input type="submit" value="投稿" />
	</form:form>

	<c:if test="${not empty textList}">
		<tbody>
			<c:forEach var="text" items="${textList}">
					<c:out value="${text.memo}"></c:out><br /><br />
			</c:forEach>
		</tbody>
	</c:if>
</body>
</html>