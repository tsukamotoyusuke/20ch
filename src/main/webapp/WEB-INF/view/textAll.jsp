<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body>
	<h1>${title}</h1>
	<a href="insert/input/">投稿画面</a>
	<h2>${title2}</h2>
	<c:forEach items="${texts}" var="text">
		<p>投稿内容<br />・
		<c:out value="${text.memo}"></c:out><br /><br />コメント内容
				<c:forEach items="${comments}" var="comment1">
					<c:if test="${comment1.memoId == text.id }"><br />
						・<c:out value="${comment1.comment}"></c:out>
					</c:if>
				</c:forEach>

				<form:form modelAttribute="textCommentForm">
					<form:label path="comment">コメント投稿内容</form:label><br />
					<form:textarea path="comment" cols="80" rows="5" /><br />
					<input type="submit" value="コメント投稿">
					<form:hidden path="memoId" value="${text.id}" />
				</form:form>
		</p>
	</c:forEach>
</body>
</html>