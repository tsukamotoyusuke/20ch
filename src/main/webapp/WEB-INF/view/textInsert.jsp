<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>Welcome</title>
</head>
<body>
	<h1>${message}</h1>
	<form:form modelAttribute="textForm">
		<form:label path="memo">投稿内容</form:label><br />
		<form:textarea path="memo" cols="80" rows="5" /><br />
		<input type="submit" value="投稿">
	</form:form>
</body><br />
<a href="/SpringKenshu/20ch/">投稿一覧画面に戻る</a>
</html>